'use strict';

process.env.NODE_ENV = 'test';

let expect = require('chai').expect;
let parser = require('./index');

const text = `Some simple text, 
with some different words, 
that sometimes may repeat and sometimes not for some reasons.`;

describe('words-count', () =>{
    it('should parse text and return array of different words', () => {
        let words = parser.parseWords(text);
        
        expect(words).to.deep.equal([
            'some', 'simple', 'text', 'with', 'some', 'different',
            'words', 'that', 'sometimes', 'may',
            'repeat', 'and', 'sometimes','not', 'for', 'some', 'reasons'
        ])
    });

    it('should reduce words', () => {
        let words = parser.parseWords(text);
        let reduced = parser.reduceWords(words);

        expect(reduced).to.deep.equal({
            'some': 3,
            'simple': 1,
            'text': 1,
            'with': 1,
            'different': 1,
            'words': 1,
            'that': 1,
            'sometimes': 2,
            'may': 1,
            'repeat': 1,
            'and': 1,
            'not': 1,
            'for': 1,
            'reasons': 1
        })
    });

    it('should sort words by count', () => {
        let words = parser.parseWords(text);
        let reduced = parser.reduceWords(words);
        let sorted = parser.sortWords(reduced);

        expect(sorted).to.deep.equal([
            {word: 'some', count: 3},
            {word: 'sometimes', count: 2},
            {word: 'may', count: 1},
            {word: 'text', count: 1},
            {word: 'different', count: 1},
            {word: 'words', count: 1},
            {word: 'that', count: 1},
            {word: 'simple', count: 1},
            {word: 'with', count: 1},
            {word: 'repeat', count: 1},
            {word: 'and', count: 1},
            {word: 'not', count: 1},
            {word: 'for', count: 1},
            {word: 'reasons', count: 1}
        ])
    });
});