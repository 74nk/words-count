'use strict';

let _ = require('lodash');

function parseWords(text) {
    let words = _.words(text);

    return words.map(w => w.toLowerCase());
}

function reduceWords(words) {
    return words.reduce((ret, word) => {
        ret[word] = ret[word] ? ret[word] + 1 : 1;
        return ret;
    }, {})
}

function sortWords(words) {
    return _.keys(words).sort((a, b) => words[b] - words[a]).map(word => ({word, count: words[word]}))
}

module.exports = {
    parseWords,
    reduceWords,
    sortWords
};